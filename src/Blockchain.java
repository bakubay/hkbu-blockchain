import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;

public class Blockchain {
    private ArrayList<Block> blockchain = new ArrayList<>();
    public  HashMap<String, TxOut> UTXOs = new HashMap<String, TxOut>();
    public  float minimumTransaction = 0;
    public User userA = new User();
    public User coinBaseUser = new User();

    private int BLOCK_GENERATION_INTERVAL = 10;
    private int DIFFICULTY_ADJUSTMENT_INTERVAL = 10;
    private int INITIAL_DIFFICULTY = 4;



    public Block generateGenesisBlock(){
        Transaction genesisTransaction = new Transaction(coinBaseUser, userA.publicKey);
        genesisTransaction.generateSignature(coinBaseUser.privateKey);
		genesisTransaction.transactionId = "0";
		genesisTransaction.txOuts.add(new TxOut(genesisTransaction.reciepient, genesisTransaction.amount, genesisTransaction.transactionId));
		UTXOs.put(genesisTransaction.txOuts.get(0).id, genesisTransaction.txOuts.get(0));

        ArrayList<Transaction> transactions = new ArrayList<>();
        transactions.add(genesisTransaction);

        Block genesisBlock = new Block(0, generateTimestamp(), "0", "", INITIAL_DIFFICULTY, 0 , transactions);

        return genesisBlock;
    }

    public boolean addBlock(Block newBlock){
        blockchain.add(newBlock);
        return true;
    }


    public Block generateNextBlock(){
        Transaction coinbaseTransaction = new Transaction(coinBaseUser, userA.publicKey);
        coinbaseTransaction.processCoinbaseTransaction(this, coinBaseUser);

        ArrayList<Transaction> transactions = new ArrayList<>();
        transactions.add(coinbaseTransaction);


        int index = generateIndex();

        long timestamp = generateTimestamp();
        int difficulty = getDifficulty();
        Block prevBlock = getLastBlock();
        String prevHash = prevBlock == null ? "0" : prevBlock.getHash();

        Block newBlock = findBlock(index, prevHash,timestamp,difficulty,transactions);
        //broadcastLatest;
        return newBlock;
    }


    public int generateIndex() {
        return this.blockchain.size();
    }

    public int getSize(){
        return this.blockchain.size();
    }

    private Block getLastBlock() {
        int size = this.blockchain.size();
        return size == 0 ? null : this.blockchain.get(size - 1);
    }

    public Block findBlock(int index, String prevHash, long timestamp, int difficulty, ArrayList<Transaction> transactions) {
        int nonce = 0;
        while (true) {
            String hash = calculateHash(index, prevHash, timestamp, difficulty, nonce, transactions);
            if (hashMatchesDifficulty(hash, difficulty)) {
                System.out.print("\nBlock Mined!!! : " + hash);
                return new Block(index, timestamp, hash, prevHash, difficulty, nonce, transactions);
            }
            nonce++;
        }
    }
    //calculateHash(nextIndex,previousBlock.hash,nextTimestamp,blockData);

    public String calculateHash(int index, String previousHash, long timeStamp,int difficulty, int nonce, ArrayList<Transaction> transactions) {
        String calculatedhash = Encryption.applySha256( index+
                previousHash +
                timeStamp +
                difficulty+
                nonce + transactions);
        return calculatedhash;
    }

    private long generateTimestamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant instant = timestamp.toInstant();
        return instant.toEpochMilli();
    }

    public int getDifficulty() {
        Block lastBlock = this.getLastBlock();
        if (lastBlock == null) return 5;
        if (lastBlock.getIndex() % DIFFICULTY_ADJUSTMENT_INTERVAL == 0 && lastBlock.getIndex() != 0) {
            return getAdjustedDifficulty(lastBlock);
        }
        return lastBlock.difficulty;
    }


    public int getAdjustedDifficulty(Block lastBlock) {
        Block prevAdjustmentBlock = this.blockchain.get(this.generateIndex() - DIFFICULTY_ADJUSTMENT_INTERVAL);
        long timeExpected = BLOCK_GENERATION_INTERVAL * DIFFICULTY_ADJUSTMENT_INTERVAL;
        long timeTaken = lastBlock.getTimeStamp() - prevAdjustmentBlock.getTimeStamp();
        if (timeTaken < timeExpected / 2) {
            return  prevAdjustmentBlock.getDifficulty() + 1;
        }
        if (timeTaken > timeExpected * 2) {
            return prevAdjustmentBlock.getDifficulty() - 1;
        }
        return prevAdjustmentBlock.getDifficulty();
    }


    public boolean hashMatchesDifficulty(String hash, int difficulty) {
        String requiredPrefix = repeat('0', difficulty);
        return hash.startsWith(requiredPrefix);
    }


    private String repeat(char c, int count) {
        String result = "";
        for (int i = 0; i < count; i++) {
            result += c;
        }
        return result;
    }

    public Block getGenesisBlock(){
        if(this.blockchain.size() > 0) {
            return this.blockchain.get(0);
        }
        return generateGenesisBlock();
    }



    public  Boolean isChainValid() {
        Block currentBlock;
        Block previousBlock;

        String hashTarget = new String(new char[getDifficulty()]).replace('\0', '0');
        for(int i=1; i < blockchain.size(); i++) {

            currentBlock = blockchain.get(i);
            previousBlock = blockchain.get(i-1);

            boolean a = currentBlock.isValidBlock(previousBlock);

            if (!currentBlock.isValidBlock(previousBlock)){
                System.out.print("\nNot valid");
                return false;
            }

            if(!currentBlock.hash.substring( 0, getDifficulty()).equals(hashTarget)) {
                System.out.println("#This block hasn't been mined");
                return false;
            }
        }
        System.out.println("\nBlockchain is valid");
        return true;
    }

}
