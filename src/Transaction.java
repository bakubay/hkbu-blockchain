import java.security.*;
import java.util.ArrayList;

public class Transaction {
	
	public String transactionId;
	public ArrayList<TxIn> txIns = new ArrayList<TxIn>();
	public ArrayList<TxOut> txOuts = new ArrayList<TxOut>();
	public PublicKey sender;
	public PublicKey reciepient;
	public float amount;
	public byte[] signature;



	private static int sequence = 0;

	private int COINBASE_AMOUNT = 50;


	// Constructor:
	public Transaction(PublicKey from, PublicKey to, float amount, ArrayList<TxIn> inputs) {
		this.sender = from;
		this.reciepient = to;
		this.amount = amount;
		this.txIns = inputs;
	}


	public Transaction(User coinbaseUser, PublicKey to){
		this.sender = coinbaseUser.publicKey;
		this.reciepient = to;
		this.amount = COINBASE_AMOUNT;

		TxIn txIn = new TxIn("");
		ArrayList<TxIn> txins = new ArrayList<>();
		txins.add(txIn);
		this.txIns = txins;
	}

	public boolean processCoinbaseTransaction(Blockchain blockchain, User coinbaseUser){
		this.generateSignature(coinbaseUser.privateKey);
		transactionId = calulateHash();

		txOuts.add(new TxOut(this.reciepient, amount, transactionId));

		for(TxOut o : txOuts) {
			blockchain.UTXOs.put(o.id , o);
		}

		return true;

	}
	
	public boolean processTransaction(Blockchain blockchain) {
		
		if(verifySignature() == false) {
			System.out.println("ERROR: Failed to verify transaction signature");
			return false;
		}
				
		for(TxIn i : txIns) {
			i.UTXO = blockchain.UTXOs.get(i.transactionOutputId);
		}

		if(getInputsValue() < blockchain.minimumTransaction) {
			System.out.println("ERROR: Transaction amount insufficient: " + getInputsValue());
			System.out.println("Please enter the amount greater than " + blockchain.minimumTransaction);
			return false;
		}
		
		float leftOver = getInputsValue() - amount;
		transactionId = calulateHash();

		txOuts.add(new TxOut( this.reciepient, amount, transactionId));
		txOuts.add(new TxOut( this.sender, leftOver, transactionId));
				
		for(TxOut o : txOuts) {
			blockchain.UTXOs.put(o.id , o);
		}
		
		for(TxIn i : txIns) {
			if(i.UTXO == null) continue;
			blockchain.UTXOs.remove(i.UTXO.id);
		}
		
		return true;
	}

	public ArrayList<TxIn> getTxIns() {
		return txIns;
	}


	public ArrayList<TxOut> getTxOuts() {
		return txOuts;
	}
	
	public float getInputsValue() {
		float total = 0;
		for(TxIn i : txIns) {
			if(i.UTXO == null) continue; //if Transaction can't be found skip it, This behavior may not be optimal.
			total += i.UTXO.value;
		}
		return total;
	}
	
	public void generateSignature(PrivateKey privateKey) {
		String data = Encryption.getStringFromKey(sender) + Encryption.getStringFromKey(reciepient) + Float.toString(amount)	;
		signature = Encryption.applyECDSASig(privateKey,data);
	}
	
	public boolean verifySignature() {
		String data = Encryption.getStringFromKey(sender) + Encryption.getStringFromKey(reciepient) + Float.toString(amount)	;
		return Encryption.verifyECDSASig(sender, data, signature);
	}
	
	public float getOutputsValue() {
		float total = 0;
		for(TxOut o : txOuts) {
			total += o.value;
		}
		return total;
	}
	
	private String calulateHash() {
		sequence++;
		return Encryption.applySha256(
				Encryption.getStringFromKey(sender) +
				Encryption.getStringFromKey(reciepient) +
				Float.toString(amount) + txIns.toString()
				);
	}

	@Override
	public String toString() {
		return String.format("Transaction: " + sender + " -> " + reciepient + "  \namount:" + amount+"\n");
	}
}
