import java.util.ArrayList;
import java.util.Date;

public class Block {

	public int index;
	public String hash;
	public String previousHash;
	public ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	public long timeStamp;
	public int nonce;
	public int difficulty;
	
	public Block(String previousHash ) {
		this.previousHash = previousHash;
		this.timeStamp = new Date().getTime();
		
		this.hash = calculateHash();
	}

	public Block(int index, long timestamp, String hash, String prevHash, int difficulty, int nonce, ArrayList<Transaction> transactions) {
		this.index = index;
		this.timeStamp = timestamp;
		this.hash = hash;
		this.previousHash = prevHash;
		this.transactions = transactions;
		this.difficulty = difficulty;
		this.nonce = nonce;
	}


	public String calculateHash() {
		String calculatedhash = Encryption.applySha256( index+
				previousHash +
				timeStamp +
				difficulty+
				nonce + transactions);
		return calculatedhash;
	}

	public int getIndex() {
		return index;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public String getHash() {
		return hash;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public int getNonce() {
		return nonce;
	}

	public String getPreviousHash() {
		return previousHash;
	}

	public boolean isValidBlock(Block previousBlock) {
		int i = previousBlock.getIndex();
		int j = this.getIndex();

		String a = previousBlock.getHash();
		String b = this.getPreviousHash();

		return (previousBlock.getIndex() + 1 == this.getIndex())
				&& (previousBlock.getHash().equals(this.getPreviousHash()));
	}

	public boolean addTransaction(Blockchain blockchain, Transaction transaction) {
		if(transaction == null) return false;
		if((!"0".equals(previousHash))) {
			if((transaction.processTransaction(blockchain) != true)) {
				System.out.println("Transaction failed to process. Discarded.");
				return false;
			}
		}

		transactions.add(transaction);
		System.out.println("Transaction Successfully added to Block");
		return true;
	}

	@Override
	public String toString() {
		return String.format("\nindex: "+ getIndex() + " \nhash: " + getHash() + "\npreviousHash: " +getPreviousHash()+ "\nTransactions: " +prettyPrint(transactions)+ "\nTimestamp:" + getTimeStamp()+  "\nnonce: "+ getNonce() + "\ndifficulty: " + getDifficulty() + "\n");
	}

	private String prettyPrint(ArrayList<Transaction> transactions){
		String transactionsString = "";
		for (Transaction i: transactions){
			transactionsString += i.toString();
		}
		return transactionsString;
	}
	
}